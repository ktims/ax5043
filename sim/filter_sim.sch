<Qucs Schematic 0.0.20>
<Properties>
  <View=0,-111,1264,1209,1.125,0,0>
  <Grid=10,10,1>
  <DataSet=filter_sim.dat>
  <DataDisplay=filter_sim.dpl>
  <OpenDisplay=1>
  <Script=filter_sim.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <.ID -30 114 SUB>
  <Rectangle -50 -30 220 190 #000000 0 1 #c0c0c0 1 0>
</Symbol>
<Components>
  <Pac P2 1 1130 350 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 1130 380 0 0 0 0>
  <.SP SP1 1 90 160 0 59 0 0 "lin" 1 "50 MHz" 1 "300 MHz" 1 "1001" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Pac P1 1 560 350 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 560 380 0 0 0 0>
  <Eqn Eqn1 1 270 170 -31 15 0 0 "H=dB(S)" 1 "Zin=real(rtoz(S[1,1]))" 1 "Zout=real(rtoz(S[2,2]))" 1 "yes" 0>
  <Sub SUB1 1 780 300 -26 108 0 0 "op_filter.sch" 0>
</Components>
<Wires>
  <900 320 1130 320 "" 0 0 0 "">
  <560 320 720 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
