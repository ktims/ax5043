<Qucs Schematic 0.0.20>
<Properties>
  <View=0,69,1436,680,1.125,0,0>
  <Grid=10,10,1>
  <DataSet=op_filter.dat>
  <DataDisplay=op_filter.dpl>
  <OpenDisplay=1>
  <Script=op_filter.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <.ID -30 114 SUB>
  <.PortSym -60 20 1 180>
  <Rectangle -60 -30 180 130 #000000 0 1 #c0c0c0 1 0>
  <.PortSym 120 20 2 0>
</Symbol>
<Components>
  <L L1 1 750 320 -26 10 0 0 "68 nH" 1 "" 0>
  <L L3 1 1040 320 -26 10 0 0 "75nH" 1 "" 0>
  <C C5 1 900 350 17 -26 0 1 "30 pF" 1 "" 0 "neutral" 0>
  <GND *1 5 900 380 0 0 0 0>
  <GND * 5 590 380 0 0 0 0>
  <C C3 1 590 350 17 -26 0 1 "30 pF" 1 "" 0 "neutral" 0>
  <Port IN 1 340 320 -23 12 0 0 "1" 1 "analog" 0>
  <Port OUT 1 1340 320 4 -50 0 2 "2" 1 "analog" 0>
  <C C1 1 750 240 -26 17 0 0 "2.7 pF" 1 "" 0 "neutral" 0>
  <C C6 1 470 320 -26 17 0 0 "220 pF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <700 320 720 320 "" 0 0 0 "">
  <780 320 800 320 "" 0 0 0 "">
  <800 240 800 320 "" 0 0 0 "">
  <780 240 800 240 "" 0 0 0 "">
  <700 240 700 320 "" 0 0 0 "">
  <700 240 720 240 "" 0 0 0 "">
  <800 320 900 320 "" 0 0 0 "">
  <900 320 1010 320 "" 0 0 0 "">
  <590 320 700 320 "" 0 0 0 "">
  <1070 320 1340 320 "" 0 0 0 "">
  <340 320 440 320 "" 0 0 0 "">
  <500 320 590 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
