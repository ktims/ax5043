<Qucs Schematic 0.0.20>
<Properties>
  <View=0,0,1274,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=amplifier_sim.dat>
  <DataDisplay=amplifier_sim.dpl>
  <OpenDisplay=1>
  <Script=amplifier_sim.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 110 220 0 59 0 0 "lin" 1 "50 MHz" 1 "250 MHz" 1 "1001" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 290 230 -31 15 0 0 "H=dB(S)" 1 "Zin=real(rtoz(S[1,1]))" 1 "Zout=real(rtoz(S[2,2]))" 1 "yes" 0>
  <Pac P1 1 520 430 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <Pac P2 1 1140 430 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 520 460 0 0 0 0>
  <GND * 5 1140 460 0 0 0 0>
  <Sub SUB1 1 820 400 -26 88 0 0 "amplifier.sch" 0>
</Components>
<Wires>
  <520 400 740 400 "" 0 0 0 "">
  <940 400 1140 400 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
