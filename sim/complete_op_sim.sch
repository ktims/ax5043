<Qucs Schematic 0.0.20>
<Properties>
  <View=220,-130,2434,1087,0.763411,0,0>
  <Grid=10,10,1>
  <DataSet=complete_op_sim.dat>
  <DataDisplay=complete_op_sim.dpl>
  <OpenDisplay=1>
  <Script=amplifier_sim.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <SPfile X2 1 1610 470 -26 -89 1 2 "D_PE4251_25C_5.000V_4.000V_1.000V_RF1_SN11_A.s3p" 1 "rectangular" 0 "linear" 0 "open" 0 "3" 0>
  <GND * 5 1610 530 0 0 0 0>
  <R R1 1 1680 530 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 5 1680 560 0 0 0 0>
  <C C11 1 1740 440 -26 17 0 0 "220 pF" 1 "" 0 "neutral" 0>
  <Pac P1 1 880 470 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 880 500 0 0 0 0>
  <GND * 5 2120 500 0 0 0 0>
  <Pac P2 1 2120 470 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <.SP SP1 1 690 350 0 60 0 0 "lin" 1 "50 MHz" 1 "250 MHz" 1 "1001" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 720 240 -31 15 0 0 "H=dB(S)" 1 "Zin=rtoz(S[1,1])" 1 "Zout=rtoz(S[2,2])" 1 "yes" 0>
  <Sub AMP 1 1240 440 -26 88 0 0 "amplifier.sch" 0>
  <Sub op_filter 1 1900 420 -26 108 0 0 "op_filter.sch" 0>
</Components>
<Wires>
  <1640 500 1680 500 "" 0 0 0 "">
  <1640 440 1710 440 "" 0 0 0 "">
  <1770 440 1840 440 "" 0 0 0 "">
  <1360 440 1580 440 "" 0 0 0 "">
  <880 440 1160 440 "" 0 0 0 "">
  <2020 440 2120 440 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
